FROM node:17-alpine

WORKDIR /usr/src/app

COPY package*.json ./
RUN npm install
COPY server.js .

EXPOSE 3003
CMD ["npm", "start"]

